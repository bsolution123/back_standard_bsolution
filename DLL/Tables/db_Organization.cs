﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Tables
{
    public class db_Organization
    {
        public int Id { get; set; }
        public string Org_Name { get; set; }
        public string WorkedIn { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string AddressInDetail { get; set; }
        public string CommercialRegistrationNumber { get; set; }
        public string TaxCard { get; set; }
        public DateTime EstablishmentDate { get; set; }
        public virtual ICollection<db_Company> Companies { get; set; }
        public virtual ICollection<db_Department> Departments { get; set; }
    }
}
