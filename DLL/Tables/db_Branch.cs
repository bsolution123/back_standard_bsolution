﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Tables
{
    public class db_Branch
    {
        [Key]
        public int Id { get; set; }
        public string BranchName { get; set; }
        public bool? MainBranch { get; set; }
        public string County { get; set; }
        public string City { get; set; }
        public string AddressInDetail { get; set; }
        public bool IsActive { get; set; }
        public DateTime EstablishmentDate { get; set; }
        [ForeignKey("Company")]
        public int? Company_Id { get; set; }
        public virtual db_Company Company { get; set; }
        public virtual ICollection<db_Department> Departments { get; set; }
        [ForeignKey("Branch")]
        public int? MainBranch_Id { get; set; }
        public virtual db_Branch Branch { get; set; }
    }
}
