﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Tables
{
	public class db_IPAddress
	{
		[Key]
		public int Id { get; set; }
		public string IPAddress { get; set; }
		public string Note { get; set; }
    
    }
}
