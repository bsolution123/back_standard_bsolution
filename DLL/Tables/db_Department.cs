﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Tables
{
    public class db_Department
    {
        [Key]
        public int Id { get; set; }
        public string DepartmentName { get; set; }
        public virtual ICollection<db_Branch> Branchs { get; set; }
    }
}
