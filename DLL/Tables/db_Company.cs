﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Tables
{
    public class db_Company
    {
        [Key]
        public int Id { get; set; }
        public string Comp_Name { get; set; }
        public string WorkedIn { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string AddressInDetail { get; set; }
        public string CommercialRegistrationNumber { get; set; }
        public string TaxCard { get; set; }
        public DateTime EstablishmentDate { get; set; }
        public virtual ICollection<db_Branch> Branchs { get; set; }
        [ForeignKey("Organization")]
        public int? Organization_Id { get; set; }
        public virtual db_Organization Organization { get; set; }
    }
}
