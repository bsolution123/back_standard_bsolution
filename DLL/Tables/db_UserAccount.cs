﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Tables
{
  public class db_UserAccount
    {
        [Key]
        public int Id { get; set; }
        public string FullNameAR { get; set; }
        public string FullNameEN { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string AccessToken { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ApprovedBy  { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public bool? IsActive { get; set; }
		public bool? IsLoggedIn { get; set; }
		public string Note { get; set; }

        public int Status { get; set; }
        public string IPSignUp { get; set; }
        [ForeignKey("Role")]
        public int? Role_Id { get; set; }
        public virtual db_Role Role { get; set; }

        public virtual ICollection<db_Link> Links { get; set; }


    }

}
