﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Tables
{
	public class db_Link
	{
		[Key]
		public int Id { get; set; }
		public string LinkAddress { get; set; }
        public virtual ICollection<db_UserAccount> UserAccounts { get; set; }
    }
}
