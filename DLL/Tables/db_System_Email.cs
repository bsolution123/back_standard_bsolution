﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Tables
{
  public  class db_System_Email
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string SmtpServer { get; set; }
        public string SmtpPort { get; set; }     
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
