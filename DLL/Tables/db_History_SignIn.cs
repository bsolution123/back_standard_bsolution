﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Tables
{
   public class db_History_SignIn
    {
        [Key]
        public int Id { get; set; }
        public string IP { get; set; }
        public DateTime Date { get; set; }
        [ForeignKey("UserAccount")]
        public int? User_Id { get; set; }
        public virtual db_UserAccount UserAccount { get; set; }
    }
}
