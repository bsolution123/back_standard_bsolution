﻿
using DLL.Model_Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repository
{
    public static class Context_singletone
    {
      // Context Context = new Context();


        private static Context _ctx;
        // private Context_singletone() { }

        public static Context getInstance()
        {
            if (_ctx == null)
            {
                return _ctx = new Context();
            }
            else
            {
                return _ctx;
            }

        }



    }
}
