﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repository
{
    public  class Repository<entity> where entity :class
    {
        public void AddList(List<entity> edntity)
        {
            Context_singletone.getInstance().Set<entity>().AddRange(edntity);
        }
        public entity Add(entity entity)
        {
          return  Context_singletone.getInstance().Set<entity>().Add(entity) ;  
        }

        public void Edit(entity entity)
        {
            Context_singletone.getInstance().Entry(entity).State =EntityState.Modified;
        }

        public static IEnumerable<entity> FindBy(Expression<Func<entity, bool>> predicate)
        {
          return  Context_singletone.getInstance().Set<entity>().Where(predicate) ;
        }

        public static  List<entity>  GetAll()
        {
            return Context_singletone.getInstance().Set<entity>().ToList();
        }
        public static  entity GetById(params object[] id)
        {
            return Context_singletone.getInstance().Set<entity>().Find(id);
        }

        public  void Remove(entity entity)
        {
            Context_singletone.getInstance().Set<entity>().Remove(entity);           
        }

        public  bool  Save()
        {          
           return Context_singletone.getInstance().SaveChanges()>0;
        }
        public void Dispose()
        {
            Context_singletone.getInstance().Dispose();
        }
    
    }
}
