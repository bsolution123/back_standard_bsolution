﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repository
{
    interface IRepository<entity> where entity:class
    {
        List<entity> GetAll();


        IEnumerable<entity> FindBy(Expression<Func<entity, bool>> predicate);
        entity GetById(params object[]id);

        entity Add(entity entity);

        void Remove(entity entity);

        void Edit(entity entity);

        
    }

    
} 
    

