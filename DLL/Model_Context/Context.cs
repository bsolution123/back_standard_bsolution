﻿using DLL.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Model_Context
{
   public class Context : DbContext
    {
        public Context() : base("ERPStandards")
        {

        }
        public DbSet<db_UserAccount> users { get; set; }
        public DbSet<db_Role> roles { get; set; }
		public DbSet<db_IPAddress> IPAddresses { get; set; }
		public DbSet<db_Link> Links { get; set; }
        public DbSet<db_Department> Departments { get; set; }
        public DbSet<db_Contact> Contacts { get; set; }
        public DbSet<db_Organization> Organizations { get; set; }
        public DbSet<db_Company> Companies { get; set; }
        public DbSet<db_Branch> Branchs { get; set; }
        public DbSet<db_System_Email> System_Emails { get; set; }
        public DbSet<db_History_SignIn> History_SignIns { get; set; }
    }
}
