namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetblBranch2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.db_Branch", "MainBranch_Id", c => c.Int());
            CreateIndex("dbo.db_Branch", "MainBranch_Id");
            AddForeignKey("dbo.db_Branch", "MainBranch_Id", "dbo.db_Branch", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.db_Branch", "MainBranch_Id", "dbo.db_Branch");
            DropIndex("dbo.db_Branch", new[] { "MainBranch_Id" });
            DropColumn("dbo.db_Branch", "MainBranch_Id");
        }
    }
}
