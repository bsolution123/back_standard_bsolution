namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetblBranch1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.db_Branch", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.db_Branch", "EstablishmentDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.db_Branch", "EstablishmentDate");
            DropColumn("dbo.db_Branch", "IsActive");
        }
    }
}
