namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class q : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.db_System_Email",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Password = c.String(),
                        Port = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.db_UserAccount", "IPSignUp", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.db_UserAccount", "IPSignUp");
            DropTable("dbo.db_System_Email");
        }
    }
}
