namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateSystemEmail : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.db_System_Email", "Port", c => c.Int());
            AlterColumn("dbo.db_System_Email", "CreatedBy", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.db_System_Email", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.db_System_Email", "Port", c => c.Int(nullable: false));
        }
    }
}
