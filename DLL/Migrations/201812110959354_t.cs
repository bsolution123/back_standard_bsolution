namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class t : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.db_System_Email", "SmtpServer", c => c.String());
            AddColumn("dbo.db_System_Email", "SmtpPort", c => c.String());
            DropColumn("dbo.db_System_Email", "Port");
        }
        
        public override void Down()
        {
            AddColumn("dbo.db_System_Email", "Port", c => c.Int());
            DropColumn("dbo.db_System_Email", "SmtpPort");
            DropColumn("dbo.db_System_Email", "SmtpServer");
        }
    }
}
