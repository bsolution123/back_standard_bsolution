namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetblBranch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.db_Branch", "County", c => c.String());
            AddColumn("dbo.db_Branch", "City", c => c.String());
            AddColumn("dbo.db_Branch", "AddressInDetail", c => c.String());
            AlterColumn("dbo.db_Branch", "MainBranch", c => c.Boolean());
            DropColumn("dbo.db_Branch", "Address");
        }
        
        public override void Down()
        {
            AddColumn("dbo.db_Branch", "Address", c => c.String());
            AlterColumn("dbo.db_Branch", "MainBranch", c => c.Boolean(nullable: false));
            DropColumn("dbo.db_Branch", "AddressInDetail");
            DropColumn("dbo.db_Branch", "City");
            DropColumn("dbo.db_Branch", "County");
        }
    }
}
