namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class g : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.db_UserAccount", "PartTime");
            DropColumn("dbo.db_UserAccount", "FullTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.db_UserAccount", "FullTime", c => c.Boolean());
            AddColumn("dbo.db_UserAccount", "PartTime", c => c.Boolean());
        }
    }
}
