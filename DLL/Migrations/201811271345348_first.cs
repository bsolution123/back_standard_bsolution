namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.db_Department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_IPAddress",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IPAddress = c.String(),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_UserAccount",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullNameAR = c.String(),
                        FullNameEN = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        AccessToken = c.String(),
                        CreationDate = c.DateTime(),
                        ApprovedBy = c.Int(),
                        ApprovedDate = c.DateTime(),
                        IsActive = c.Boolean(),
                        IsLoggedIn = c.Boolean(),
                        Note = c.String(),
                        PartTime = c.Boolean(),
                        FullTime = c.Boolean(),
                        Status = c.Int(nullable: false),
                        Role_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.db_Role", t => t.Role_Id)
                .Index(t => t.Role_Id);
            
            CreateTable(
                "dbo.db_Link",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LinkAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Power = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_UserAccountdb_IPAddress",
                c => new
                    {
                        db_UserAccount_Id = c.Int(nullable: false),
                        db_IPAddress_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.db_UserAccount_Id, t.db_IPAddress_Id })
                .ForeignKey("dbo.db_UserAccount", t => t.db_UserAccount_Id, cascadeDelete: true)
                .ForeignKey("dbo.db_IPAddress", t => t.db_IPAddress_Id, cascadeDelete: true)
                .Index(t => t.db_UserAccount_Id)
                .Index(t => t.db_IPAddress_Id);
            
            CreateTable(
                "dbo.db_Linkdb_UserAccount",
                c => new
                    {
                        db_Link_Id = c.Int(nullable: false),
                        db_UserAccount_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.db_Link_Id, t.db_UserAccount_Id })
                .ForeignKey("dbo.db_Link", t => t.db_Link_Id, cascadeDelete: true)
                .ForeignKey("dbo.db_UserAccount", t => t.db_UserAccount_Id, cascadeDelete: true)
                .Index(t => t.db_Link_Id)
                .Index(t => t.db_UserAccount_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.db_UserAccount", "Role_Id", "dbo.db_Role");
            DropForeignKey("dbo.db_Linkdb_UserAccount", "db_UserAccount_Id", "dbo.db_UserAccount");
            DropForeignKey("dbo.db_Linkdb_UserAccount", "db_Link_Id", "dbo.db_Link");
            DropForeignKey("dbo.db_UserAccountdb_IPAddress", "db_IPAddress_Id", "dbo.db_IPAddress");
            DropForeignKey("dbo.db_UserAccountdb_IPAddress", "db_UserAccount_Id", "dbo.db_UserAccount");
            DropIndex("dbo.db_Linkdb_UserAccount", new[] { "db_UserAccount_Id" });
            DropIndex("dbo.db_Linkdb_UserAccount", new[] { "db_Link_Id" });
            DropIndex("dbo.db_UserAccountdb_IPAddress", new[] { "db_IPAddress_Id" });
            DropIndex("dbo.db_UserAccountdb_IPAddress", new[] { "db_UserAccount_Id" });
            DropIndex("dbo.db_UserAccount", new[] { "Role_Id" });
            DropTable("dbo.db_Linkdb_UserAccount");
            DropTable("dbo.db_UserAccountdb_IPAddress");
            DropTable("dbo.db_Role");
            DropTable("dbo.db_Link");
            DropTable("dbo.db_UserAccount");
            DropTable("dbo.db_IPAddress");
            DropTable("dbo.db_Department");
        }
    }
}
