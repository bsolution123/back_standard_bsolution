namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class asd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.db_Branch",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BranchName = c.String(),
                        MainBranch = c.Boolean(),
                        County = c.String(),
                        City = c.String(),
                        AddressInDetail = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        EstablishmentDate = c.DateTime(nullable: false),
                        Company_Id = c.Int(),
                        MainBranch_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.db_Branch", t => t.MainBranch_Id)
                .ForeignKey("dbo.db_Company", t => t.Company_Id)
                .Index(t => t.Company_Id)
                .Index(t => t.MainBranch_Id);
            
            CreateTable(
                "dbo.db_Company",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Comp_Name = c.String(),
                        WorkedIn = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        AddressInDetail = c.String(),
                        CommercialRegistrationNumber = c.String(),
                        TaxCard = c.String(),
                        EstablishmentDate = c.DateTime(nullable: false),
                        IsMainCompany = c.Boolean(),
                        Organization_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.db_Company", t => t.Organization_Id)
                .Index(t => t.Organization_Id);
            
            CreateTable(
                "dbo.db_Department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_Contact",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WebSite = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_History_SignIn",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IP = c.String(),
                        Date = c.DateTime(nullable: false),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.db_UserAccount", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.db_UserAccount",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullNameAR = c.String(),
                        FullNameEN = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        AccessToken = c.String(),
                        CreationDate = c.DateTime(),
                        ApprovedBy = c.Int(),
                        ApprovedDate = c.DateTime(),
                        IsActive = c.Boolean(),
                        IsLoggedIn = c.Boolean(),
                        Note = c.String(),
                        PartTime = c.Boolean(),
                        FullTime = c.Boolean(),
                        Status = c.Int(nullable: false),
                        IPSignUp = c.String(),
                        Role_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.db_Role", t => t.Role_Id)
                .Index(t => t.Role_Id);
            
            CreateTable(
                "dbo.db_Link",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LinkAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Power = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_IPAddress",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IPAddress = c.String(),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_System_Email",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Password = c.String(),
                        SmtpServer = c.String(),
                        SmtpPort = c.String(),
                        CreatedBy = c.Int(),
                        CreatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.db_Departmentdb_Branch",
                c => new
                    {
                        db_Department_Id = c.Int(nullable: false),
                        db_Branch_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.db_Department_Id, t.db_Branch_Id })
                .ForeignKey("dbo.db_Department", t => t.db_Department_Id, cascadeDelete: true)
                .ForeignKey("dbo.db_Branch", t => t.db_Branch_Id, cascadeDelete: true)
                .Index(t => t.db_Department_Id)
                .Index(t => t.db_Branch_Id);
            
            CreateTable(
                "dbo.db_Linkdb_UserAccount",
                c => new
                    {
                        db_Link_Id = c.Int(nullable: false),
                        db_UserAccount_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.db_Link_Id, t.db_UserAccount_Id })
                .ForeignKey("dbo.db_Link", t => t.db_Link_Id, cascadeDelete: true)
                .ForeignKey("dbo.db_UserAccount", t => t.db_UserAccount_Id, cascadeDelete: true)
                .Index(t => t.db_Link_Id)
                .Index(t => t.db_UserAccount_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.db_History_SignIn", "User_Id", "dbo.db_UserAccount");
            DropForeignKey("dbo.db_UserAccount", "Role_Id", "dbo.db_Role");
            DropForeignKey("dbo.db_Linkdb_UserAccount", "db_UserAccount_Id", "dbo.db_UserAccount");
            DropForeignKey("dbo.db_Linkdb_UserAccount", "db_Link_Id", "dbo.db_Link");
            DropForeignKey("dbo.db_Departmentdb_Branch", "db_Branch_Id", "dbo.db_Branch");
            DropForeignKey("dbo.db_Departmentdb_Branch", "db_Department_Id", "dbo.db_Department");
            DropForeignKey("dbo.db_Branch", "Company_Id", "dbo.db_Company");
            DropForeignKey("dbo.db_Company", "Organization_Id", "dbo.db_Company");
            DropForeignKey("dbo.db_Branch", "MainBranch_Id", "dbo.db_Branch");
            DropIndex("dbo.db_Linkdb_UserAccount", new[] { "db_UserAccount_Id" });
            DropIndex("dbo.db_Linkdb_UserAccount", new[] { "db_Link_Id" });
            DropIndex("dbo.db_Departmentdb_Branch", new[] { "db_Branch_Id" });
            DropIndex("dbo.db_Departmentdb_Branch", new[] { "db_Department_Id" });
            DropIndex("dbo.db_UserAccount", new[] { "Role_Id" });
            DropIndex("dbo.db_History_SignIn", new[] { "User_Id" });
            DropIndex("dbo.db_Company", new[] { "Organization_Id" });
            DropIndex("dbo.db_Branch", new[] { "MainBranch_Id" });
            DropIndex("dbo.db_Branch", new[] { "Company_Id" });
            DropTable("dbo.db_Linkdb_UserAccount");
            DropTable("dbo.db_Departmentdb_Branch");
            DropTable("dbo.db_System_Email");
            DropTable("dbo.db_IPAddress");
            DropTable("dbo.db_Role");
            DropTable("dbo.db_Link");
            DropTable("dbo.db_UserAccount");
            DropTable("dbo.db_History_SignIn");
            DropTable("dbo.db_Contact");
            DropTable("dbo.db_Department");
            DropTable("dbo.db_Company");
            DropTable("dbo.db_Branch");
        }
    }
}
