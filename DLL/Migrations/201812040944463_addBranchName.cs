namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBranchName : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.db_Departmentdb_Branch",
                c => new
                    {
                        db_Department_Id = c.Int(nullable: false),
                        db_Branch_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.db_Department_Id, t.db_Branch_Id })
                .ForeignKey("dbo.db_Department", t => t.db_Department_Id, cascadeDelete: true)
                .ForeignKey("dbo.db_Branch", t => t.db_Branch_Id, cascadeDelete: true)
                .Index(t => t.db_Department_Id)
                .Index(t => t.db_Branch_Id);
            
            CreateTable(
                "dbo.db_Departmentdb_Organization",
                c => new
                    {
                        db_Department_Id = c.Int(nullable: false),
                        db_Organization_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.db_Department_Id, t.db_Organization_Id })
                .ForeignKey("dbo.db_Department", t => t.db_Department_Id, cascadeDelete: true)
                .ForeignKey("dbo.db_Organization", t => t.db_Organization_Id, cascadeDelete: true)
                .Index(t => t.db_Department_Id)
                .Index(t => t.db_Organization_Id);
            
            AddColumn("dbo.db_Branch", "BranchName", c => c.String());
            AddColumn("dbo.db_Branch", "MainBranch", c => c.Boolean(nullable: false));
            AddColumn("dbo.db_Branch", "Address", c => c.String());
            AddColumn("dbo.db_Company", "Comp_Name", c => c.String());
            AddColumn("dbo.db_Company", "Address", c => c.String());
            AddColumn("dbo.db_Company", "WorkedIn", c => c.String());
            AddColumn("dbo.db_Organization", "Org_Name", c => c.String());
            AddColumn("dbo.db_Organization", "Address", c => c.String());
            AddColumn("dbo.db_UserAccount", "IPSignUp", c => c.String());
            AddColumn("dbo.db_Link", "LinkAddress", c => c.String());
            DropColumn("dbo.db_Link", "Note");
        }
        
        public override void Down()
        {
            AddColumn("dbo.db_Link", "Note", c => c.String());
            DropForeignKey("dbo.db_Departmentdb_Organization", "db_Organization_Id", "dbo.db_Organization");
            DropForeignKey("dbo.db_Departmentdb_Organization", "db_Department_Id", "dbo.db_Department");
            DropForeignKey("dbo.db_Departmentdb_Branch", "db_Branch_Id", "dbo.db_Branch");
            DropForeignKey("dbo.db_Departmentdb_Branch", "db_Department_Id", "dbo.db_Department");
            DropIndex("dbo.db_Departmentdb_Organization", new[] { "db_Organization_Id" });
            DropIndex("dbo.db_Departmentdb_Organization", new[] { "db_Department_Id" });
            DropIndex("dbo.db_Departmentdb_Branch", new[] { "db_Branch_Id" });
            DropIndex("dbo.db_Departmentdb_Branch", new[] { "db_Department_Id" });
            DropColumn("dbo.db_Link", "LinkAddress");
            DropColumn("dbo.db_UserAccount", "IPSignUp");
            DropColumn("dbo.db_Organization", "Address");
            DropColumn("dbo.db_Organization", "Org_Name");
            DropColumn("dbo.db_Company", "WorkedIn");
            DropColumn("dbo.db_Company", "Address");
            DropColumn("dbo.db_Company", "Comp_Name");
            DropColumn("dbo.db_Branch", "Address");
            DropColumn("dbo.db_Branch", "MainBranch");
            DropColumn("dbo.db_Branch", "BranchName");
            DropTable("dbo.db_Departmentdb_Organization");
            DropTable("dbo.db_Departmentdb_Branch");
        }
    }
}
