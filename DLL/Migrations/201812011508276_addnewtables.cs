namespace DLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addnewtables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.db_Branch",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Company_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.db_Company", t => t.Company_Id)
                .Index(t => t.Company_Id);
            
            CreateTable(
                "dbo.db_Company",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Organization_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.db_Organization", t => t.Organization_Id)
                .Index(t => t.Organization_Id);
            
            CreateTable(
                "dbo.db_Organization",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.db_Company", "Organization_Id", "dbo.db_Organization");
            DropForeignKey("dbo.db_Branch", "Company_Id", "dbo.db_Company");
            DropIndex("dbo.db_Company", new[] { "Organization_Id" });
            DropIndex("dbo.db_Branch", new[] { "Company_Id" });
            DropTable("dbo.db_Organization");
            DropTable("dbo.db_Company");
            DropTable("dbo.db_Branch");
        }
    }
}
