﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using API.BLL.UserAccount;
using API.Models.VMUserAccount;
using DLL.Model_Context;
using DLL.Repository;
using DLL.Tables;

namespace API.Controllers
{
    public class UsersAccountController : ApiController
    {
        private Repository<db_UserAccount> Rep = new Repository<db_UserAccount>();

        [HttpPost]
        [Route("api/UsersAccount/PostSignUp")]
        [ResponseType(typeof(Approved))]
        public IHttpActionResult PostSignUp(Approved UserSignUp)
        {
            db_UserAccount _UserAccount = new db_UserAccount();        
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var CheckEmail=Repository<db_UserAccount>.FindBy(c => c.Email == UserSignUp.Email).FirstOrDefault();
                if (CheckEmail == null)
                {
                    _UserAccount.IPSignUp = UserSignUp.IPSignUp;
                    _UserAccount.FullNameAR = UserSignUp.FullNameAR;
                    _UserAccount.FullNameEN = UserSignUp.FullNameEN;
                    _UserAccount.Mobile = UserSignUp.Mobile;
                    _UserAccount.Email = UserSignUp.Email;
                    _UserAccount.Password = UserSignUp.Password;
                    _UserAccount.Note = UserSignUp.Note;
                    _UserAccount.Role_Id = UserSignUp.Role_Id;
                    _UserAccount.IsActive = false;
                 
                    _UserAccount.Status = Convert.ToInt32(Status.Pending);
                    _UserAccount.CreationDate = DateTime.Now;

                    Rep.Add(_UserAccount);
                    //_UserAccount.IPAddresses.Add();
                    //_UserAccount.

                    if (Rep.Save())
                    {
                        return Ok(UserSignUp);
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    return new BadRequestErrorMessageResult("Email Already Registered",this);
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
        [HttpGet]
        [ResponseType(typeof(Approved))]
        [Route("api/UsersAccount/GetUserById")]
        public IHttpActionResult GetUserById(int Id)
        {
            try
            {
                var originalUser = Repository<db_UserAccount>.GetById(Id);

                if (originalUser == null)
                {
                    return NotFound();
                }
                return Ok(originalUser);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
        [HttpGet]
        [ResponseType(typeof(Approved))]
        [Route("api/UsersAccount/GetUserByEmail")]
        public IHttpActionResult GetUserByEmail(string Email)
        {
            try
            {
                db_UserAccount originalUser = Repository<db_UserAccount>.FindBy(c=>c.Email==Email).FirstOrDefault();
                var oldPassword = originalUser.Password;
                if (originalUser == null)
                {
                    return NotFound();
                }
                 var NewPassword = BLL_UserAccount.GeneratePassword(originalUser.FullNameEN, originalUser.Mobile);   
                if (oldPassword == NewPassword)
                {
                    var gneratepassword = NewPassword +"55";
                    originalUser.Password = gneratepassword;
                    if (Rep.Save())
                    {
                       
                        return Ok(originalUser.Password);
                    }
                }
                else
                {
                    originalUser.Password = NewPassword;
                    if (Rep.Save())
                    {
                        return Ok(originalUser.Password);
                    }
                }
              

                return BadRequest();
               
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
    }
}
