﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using DLL.Model_Context;
using DLL.Repository;
using DLL.Tables;

namespace API.Controllers
{
    public class db_UserController : ApiController
    {
        private Context db = new Context();

        // GET: api/db_User
        [Authorize(Roles = "admin")]
        [HttpGet]
        [Route("api/data/authorize")]
        public IHttpActionResult GetForAdmin()
        {
            var identity = (ClaimsIdentity)User.Identity;
            var roles = identity.Claims
                        .Where(c => c.Type == ClaimTypes.Role)
                        .Select(c => c.Value);
            return Ok("Hello " + identity.Name + " Role: " + string.Join(",", roles.ToList()));
        }
        [Route("api/data/all")]
        public IQueryable<db_UserAccount> Getusers()
        {
          var x=  Repository<db_UserAccount>.GetAll();
            return db.users;
        }

        //// GET: api/db_User/5
        //[ResponseType(typeof(db_UserAccount))]
        //public IHttpActionResult Getdb_User(int id)
        //{
        //    db_UserAccount db_User = db.users.Find(id);
        //    if (db_User == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(db_User);
        //}

        // PUT: api/db_User/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putdb_User(int id, db_UserAccount db_User)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != db_User.Id)
            {
                return BadRequest();
            }

            db.Entry(db_User).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!db_UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/db_User
        [ResponseType(typeof(db_UserAccount))]
        public IHttpActionResult Postdb_User(db_UserAccount db_User)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.users.Add(db_User);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = db_User.Id }, db_User);
        }

        // DELETE: api/db_User/5
        [ResponseType(typeof(db_UserAccount))]
        public IHttpActionResult Deletedb_User(int id)
        {
            db_UserAccount db_User = db.users.Find(id);
            if (db_User == null)
            {
                return NotFound();
            }

            db.users.Remove(db_User);
            db.SaveChanges();

            return Ok(db_User);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool db_UserExists(int id)
        {
            return db.users.Count(e => e.Id == id) > 0;
        }
    }
}