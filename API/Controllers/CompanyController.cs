﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models.VMOrganization;
using DLL.Repository;
using DLL.Tables;

namespace API.Controllers
{
    public class CompanyController : ApiController
    {
        private Repository<db_Company> Rep = new Repository<db_Company>();
        private Repository<db_Branch> RepBranch = new Repository<db_Branch>();

        //[HttpGet]
        //[Route("api/Company/GetAllOrgnizations")]
        //[ResponseType(typeof(VmCompany))]
        //public IHttpActionResult GetAllOrgnizations()
        //{
        //    List<VmCompany> vmCompanies = null;
        //    try
        //    {
        //        vmCompanies = Repository<db_Company>.FindBy(C => C.IsMainCompany == true).Select(Com => new VmCompany()
        //        {
        //            Id = Com.Id,
        //            Comp_Name = Com.Comp_Name,
        //            Country = Com.Country,
        //            City = Com.City,
        //            AddressInDetail = Com.AddressInDetail,
        //            CommercialRegistrationNumber = Com.CommercialRegistrationNumber,
        //            WorkedIn = Com.WorkedIn,
        //            EstablishmentDate = Com.EstablishmentDate,
        //            IsMainCompany = Com.IsMainCompany,
        //            TaxCard = Com.TaxCard,
        //            Organization_Id = Com.Organization_Id,
        //        }).ToList();

        //        if (vmCompanies.Count() == 0)
        //        {
        //            return NotFound();
        //        }
        //        return Ok(vmCompanies);
        //    }
        //    catch (Exception Ex)
        //    {
        //        return BadRequest(Ex.ToString());
        //    }
        //}


        [HttpGet]
        [Route("api/Company/GetAllVmCompanies")]
        [ResponseType(typeof(VmCompany))]
        public IHttpActionResult GetAllVmCompanies()
        {
            List<VmCompany> vmCompanies = null;
            try
            {
                vmCompanies = Repository<db_Company>.GetAll().Select(C => new VmCompany()
                {
                    Id = C.Id,
                    Comp_Name = C.Comp_Name,
                    Country = C.Country,
                    City = C.City,
                    AddressInDetail = C.AddressInDetail,
                    CommercialRegistrationNumber = C.CommercialRegistrationNumber,
                    WorkedIn = C.WorkedIn,
                    EstablishmentDate = C.EstablishmentDate,
                    TaxCard = C.TaxCard,
                    Organization_Id = C.Organization_Id
                }).ToList();

                if (vmCompanies.Count() == 0)
                {
                    return NotFound();
                }

                return Ok(vmCompanies);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Company/PostOrganization")]
        [ResponseType(typeof(VmCompany))]
        public IHttpActionResult PostOrganization(VmCompany OrganizationModel)
        {
            db_Company _Company = new db_Company();
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _Company.Comp_Name = OrganizationModel.Comp_Name;
                _Company.Country = OrganizationModel.Country;
                _Company.City = OrganizationModel.City;
                _Company.AddressInDetail = OrganizationModel.AddressInDetail;
                _Company.CommercialRegistrationNumber = OrganizationModel.CommercialRegistrationNumber;
                _Company.WorkedIn = OrganizationModel.WorkedIn;
                _Company.EstablishmentDate = OrganizationModel.EstablishmentDate;
                _Company.TaxCard = OrganizationModel.TaxCard;
                _Company.Organization_Id = OrganizationModel.Organization_Id;

                Rep.Add(_Company);

                if (Rep.Save())
                {
                    return Ok(_Company);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Company/PostCompanyWithMainBranch")]
        [ResponseType(typeof(VmCompany))]
        public IHttpActionResult PostCompanyWithMainBranch(VmCompany CompanyModel)
        {
            db_Company _Company = new db_Company();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _Company.Comp_Name = CompanyModel.Comp_Name;
                _Company.Country = CompanyModel.Country;
                _Company.City = CompanyModel.City;
                _Company.AddressInDetail = CompanyModel.AddressInDetail;
                _Company.CommercialRegistrationNumber = CompanyModel.CommercialRegistrationNumber;
                _Company.WorkedIn = CompanyModel.WorkedIn;
                _Company.EstablishmentDate = CompanyModel.EstablishmentDate;
                _Company.TaxCard = CompanyModel.TaxCard;
                _Company.Organization_Id = CompanyModel.Organization_Id;

                Rep.Add(_Company);

                if (_Company != null)
                {
                    db_Branch _Branch = new db_Branch();

                    _Branch.BranchName = CompanyModel.Branch.BranchName;
                    _Branch.County = CompanyModel.Branch.County;
                    _Branch.City = CompanyModel.Branch.City;
                    _Branch.AddressInDetail = CompanyModel.Branch.AddressInDetail;
                    _Branch.EstablishmentDate = CompanyModel.Branch.EstablishmentDate;
                    _Branch.IsActive = CompanyModel.Branch.IsActive;
                    _Branch.MainBranch = true;
                    _Branch.MainBranch_Id = CompanyModel.Branch.MainBranch_Id;
                    _Branch.Company_Id = _Company.Id;
                    RepBranch.Add(_Branch);

                    if (Rep.Save())
                    {
                        return Ok(_Company);
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }

        [HttpGet]
        [ResponseType(typeof(VmCompany))]
        [Route("api/Company/GetCompanyById")]
        public IHttpActionResult GetCompanyById(int id)
        {
            try
            {
                var OriginalCompany = Repository<db_Company>.GetById(id);
                if (OriginalCompany == null)
                {
                    return NotFound();
                }
                return Ok(OriginalCompany);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }

        [HttpPut]
        [ResponseType(typeof(VmCompany))]
        [Route("api/Company/PutCompany")]
        public IHttpActionResult PutCompany(VmCompany PutCompany)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Not a valid data");
                }
                var OriginalCompany = Repository<db_Company>.GetById(PutCompany.Id);
                if (OriginalCompany != null)
                {
                    return Ok(OriginalCompany);
                }
                return Ok();
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
    }
}
