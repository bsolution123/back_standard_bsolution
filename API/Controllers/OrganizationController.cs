﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models.VMOrganization;
using DLL.Repository;
using DLL.Tables;

namespace API.Controllers
{
    public class OrganizationController : ApiController
    {
        private Repository<db_Organization> Rep = new Repository<db_Organization>();
        private Repository<db_Company> RepCompany = new Repository<db_Company>();

        [HttpGet]
        [Route("api/Organization/GetAllOrganizations")]
        [ResponseType(typeof(VmOrganization))]
        public IHttpActionResult GetAllOrganizations()
        {
            List<VmOrganization> vmOrganizations = null;
            try
            {
                vmOrganizations = Repository<db_Organization>.GetAll().Select(C => new VmOrganization()
                {
                    Id = C.Id,
                    Org_Name = C.Org_Name,
                    Country = C.Country,
                    City = C.City,
                    AddressInDetail = C.AddressInDetail,
                    CommercialRegistrationNumber = C.CommercialRegistrationNumber,
                    EstablishmentDate = C.EstablishmentDate,
                    TaxCard = C.TaxCard,
                    WorkedIn = C.WorkedIn,
                    Companies = C.Companies.Select(a => new VmCompany()
                    {
                        Id = a.Id,
                        Comp_Name = a.Comp_Name,
                        WorkedIn = a.WorkedIn,
                        Country = a.Country,
                        City = a.City,
                        AddressInDetail = a.AddressInDetail,
                        CommercialRegistrationNumber = a.CommercialRegistrationNumber,
                        EstablishmentDate = a.EstablishmentDate,
                        TaxCard = a.TaxCard,
                        Organization_Id = a.Organization_Id
                    }).ToList()
                }).ToList();

                if (vmOrganizations.Count() == 0)
                {
                    return NotFound();
                }

                return Ok(vmOrganizations);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Organization/PostOrganization")]
        [ResponseType(typeof(VmOrganization))]
        public IHttpActionResult PostOrganization(VmOrganization OrganizationModel)
        {
            db_Organization _Organization = new db_Organization();
            
            try
            {
                if (ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                _Organization.Org_Name = OrganizationModel.Org_Name;
                _Organization.Country = OrganizationModel.Country;
                _Organization.City = OrganizationModel.City;
                _Organization.AddressInDetail = OrganizationModel.AddressInDetail;
                _Organization.WorkedIn = OrganizationModel.WorkedIn;
                _Organization.CommercialRegistrationNumber = OrganizationModel.CommercialRegistrationNumber;
                _Organization.EstablishmentDate = OrganizationModel.EstablishmentDate;
                _Organization.TaxCard = OrganizationModel.TaxCard;

                Rep.Add(_Organization);

                if (_Organization != null)
                {
                    foreach (var company in OrganizationModel.Companies)
                    {
                        db_Company _Company = new db_Company();
                        _Company.Comp_Name = company.Comp_Name;
                        _Company.WorkedIn = company.WorkedIn;
                        _Company.Country = company.Country;
                        _Company.City = company.City;
                        _Company.AddressInDetail = company.AddressInDetail;
                        _Company.CommercialRegistrationNumber = company.CommercialRegistrationNumber;
                        _Company.TaxCard = company.TaxCard;
                        _Company.EstablishmentDate = company.EstablishmentDate;
                        _Company.Organization_Id = _Organization.Id;

                        RepCompany.Add(_Company);
                    }
                    
                    if (Rep.Save())
                    {
                        return Ok(_Organization);
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
    }
}
