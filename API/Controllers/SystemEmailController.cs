﻿using API.Models.VMSystemEmail;
using DLL.Repository;
using DLL.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers
{
    public class SystemEmailController : ApiController
    {
        private Repository<db_System_Email> Rep = new Repository<db_System_Email>();
        [HttpGet]
        [ResponseType(typeof(VmAPISystemEmail))]
        [Route("api/SystemEmail/GetAllEmail")]
        public IHttpActionResult GetAllEmail()
        {
            try
            {
                List<VmAPISystemEmail> Emails = null;

                Emails = Repository<db_System_Email>.GetAll().Select(C => new VmAPISystemEmail()
                {
                    Id = C.Id,
                    Email = C.Email,
                    Password = C.Password,
                    SmtpPort = C.SmtpPort
                }).ToList();

                if (Emails.Count() == 0)
                {
                    return NotFound();
                }

                return Ok(Emails);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
        [HttpPost]
        [ResponseType(typeof(VmAPISystemEmail))]
     //   [Route("api/SystemEmail/AddEmail")]
        public IHttpActionResult PostAddSystemEmail(VmAPISystemEmail systemEmailModel)
        {
            db_System_Email _System_Email = new db_System_Email();
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                _System_Email.Email = systemEmailModel.Email;
                _System_Email.Password = systemEmailModel.Password;
                _System_Email.SmtpPort = systemEmailModel.SmtpPort;
                _System_Email.SmtpServer = systemEmailModel.SmtpServer;
                
                _System_Email.CreatedDate = DateTime.Now;
                Rep.Add(_System_Email);
                if (Rep.Save())
                {
                    return Ok(_System_Email);
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception Ex)
            {

                return BadRequest(Ex.ToString());
            }
         
        }
        [HttpGet]
        [ResponseType(typeof(VmAPISystemEmail))]
        [Route("api/SystemEmail/GetEmailById")]
        public IHttpActionResult GetEmailById(int id)
        {
            try
            {
                var OriginalEmail = Repository<db_System_Email>.GetById(id);
                if (OriginalEmail == null)
                {
                    return NotFound();
                }
                return Ok(OriginalEmail);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
        [HttpPut]
        [ResponseType(typeof(VmAPISystemEmail))]
        [Route("api/SystemEmail/PutEmail")]
        public IHttpActionResult PutEmail(VmAPISystemEmail systemEmail)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest("Not a valid data");
                var OrignalEmail = Repository<db_System_Email>.GetById(systemEmail.Id);

                if (OrignalEmail != null)
                {
                    OrignalEmail.Password = systemEmail.Password;
                    OrignalEmail.Email = systemEmail.Email;
                    OrignalEmail.SmtpPort =systemEmail.SmtpPort;
                    if (Rep.Save())
                    {
                        return Ok(OrignalEmail);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                return Ok();
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
        
        [Route("api/SystemEmail/DeleteEmailSystem")]
        public IHttpActionResult DeleteEmailSystem(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest("Not a valid email id");
                }
                else
                {
                    var emailToBeDelete = Repository<db_System_Email>.GetById(id);
                    if (emailToBeDelete != null)
                    {
                        Rep.Remove(emailToBeDelete);
                        Rep.Save();
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                return Ok();
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }

        [HttpGet]
        [ResponseType(typeof(VmAPISystemEmail))]
        [Route("api/SystemEmail/SelectEmailForSendPassword")]
        public IHttpActionResult SelectEmailForSendPassword(int id)
        {
            try
            {
                var SelectEmail = Repository<db_System_Email>.GetById(id);
                if (SelectEmail == null)
                {
                    return NotFound();
                }
                return Ok(SelectEmail);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }


    }
}
