﻿using API.BLL.UserAccount;
using API.Models.VMApproved;
using API.Models.VMUserAccount;
using DLL.Repository;
using DLL.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;

namespace API.Controllers
{
    public class AdminApproveController : ApiController
    {
        private Repository<db_UserAccount> Rep = new Repository<db_UserAccount>();
        [HttpGet]
        [ResponseType(typeof(Models.VMUserAccount.Approved))]
        [Route("api/AdminApprove/GetAllPendingUsers")]
        public IHttpActionResult GetAllPendingUsers()
        {
            try
            {
                List<Models.VMApproved.Approved> Users = null;

                Users = Repository<db_UserAccount>.FindBy(C => C.Status == (int)Status.Pending)
                    .Select(C => new Models.VMApproved.Approved()
                    {
                        Id = C.Id,
                        FullNameAR = C.FullNameAR,
                        FullNameEN = C.FullNameEN,
                        Email = C.Email,
                        Mobile = C.Mobile,
                    
                    
                        Note = C.Note,
                   
                        Status = C.Status,
                        Role = C.Role_Id,
                    }).ToList();

                if (Users.Count() == 0)
                {
                    return NotFound();
                }
                return Ok(Users);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }

        [HttpGet]
        [ResponseType(typeof(Models.VMApproved.Approved))]
        [Route("api/AdminApprove/GetAllAPProvedUsers")]
        public IHttpActionResult GetAllAPProvedgUsers()
        {
            try
            {
                List<Models.VMApproved.Approved> Users = null;

                Users = Repository<db_UserAccount>.FindBy(C => C.Status == (int)Status.Approved)
                    .Select(C => new Models.VMApproved.Approved()
                    {
                        Id = C.Id,
                        FullNameAR = C.FullNameAR,
                        FullNameEN = C.FullNameEN,
                        Email = C.Email,
                        Mobile = C.Mobile,
                       

                        Note = C.Note,
                     
                        Status = C.Status,
                        Role = C.Role_Id,
                    }).ToList();

                if (Users.Count() == 0)
                {
                    return NotFound();
                }
                return Ok(Users);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
        [HttpGet]
        [ResponseType(typeof(Models.VMApproved.Approved))]
        [Route("api/AdminApprove/GetAllRejectUsers")]
        public IHttpActionResult GetAllRejectUsers()
        {
            try
            {
                List<Models.VMApproved.Approved> Users = null;

                Users = Repository<db_UserAccount>.FindBy(C => C.Status == (int)Status.Rejected)
                    .Select(C => new Models.VMApproved.Approved()
                    {
                        Id = C.Id,
                        FullNameAR = C.FullNameAR,
                        FullNameEN = C.FullNameEN,
                        Email = C.Email,
                        Mobile = C.Mobile,


                        Note = C.Note,

                        Status = C.Status,
                        Role = C.Role_Id,
                    }).ToList();

                if (Users.Count() == 0)
                {
                    return NotFound();
                }
                return Ok(Users);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
        [HttpPost]
        [Route("api/AdminApprove/PostApproved")]
        [ResponseType(typeof(Models.VMApproved.Approved))]
        public IHttpActionResult PostApproved(Models.VMApproved.Approved model)
        {
           
            try
            {
               var _UserAccount =  Repository<db_UserAccount>.GetById(model.Id);
                if (_UserAccount == null)
                {
                    return BadRequest("Not Found");
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                    _UserAccount.IPSignUp = model.IPSignUp;
                    _UserAccount.FullNameAR = model.FullNameAR;
                    _UserAccount.FullNameEN = model.FullNameEN;
                    _UserAccount.Mobile = model.Mobile;
                    _UserAccount.Email = model.Email;                
                    _UserAccount.Note = model.Note;
                    _UserAccount.Role_Id = model.Role;
                  
               
                if (model.Status==1 ||model.Status==0)
                {
                    _UserAccount.IsActive = false;
                    _UserAccount.Password = null;
                }
                else
                {
                    _UserAccount.IsActive = true;
                    _UserAccount.Password = BLL_UserAccount.GeneratePassword(model.FullNameEN, model.Mobile);
                }
                    _UserAccount.Status = model.Status;
                    _UserAccount.CreationDate = DateTime.Now;
                     Rep.Edit(_UserAccount);
                    if (Rep.Save())
                    {
                    if (_UserAccount.Password !=null)
                    {
                        return Ok(_UserAccount.Password);
                    }
                        return Ok();
                    }
                    else
                    {
                        return BadRequest();
                    }
                  
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.ToString());
            }
        }
    }
}
