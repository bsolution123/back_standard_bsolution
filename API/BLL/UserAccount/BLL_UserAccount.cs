﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace API.BLL.UserAccount
{
    enum Status
    {
        Pending = 0,
        Rejected = 1,
        Approved = 2,
       
    }
    public class BLL_UserAccount
    {
        public static string GeneratePassword(string UserName, string Mobile)
        {
            string generatedPassword = "";
            StringBuilder pass = new StringBuilder();
            pass.Append("BS");

            string name = UserName.Substring(0, 2);
            pass.Append(name);

            string Phone = Mobile.Substring(3, 2);
            pass.Append(Phone);

            int length = Convert.ToInt32(Mobile.LongCount());
            string Phone1 = Mobile.Substring(length - 2, 2);
            pass.Append(Phone1);
            
            generatedPassword = pass.ToString();
         
            pass.Append(Phone1);
            generatedPassword = pass.ToString();
            return generatedPassword;
        }
    }
}