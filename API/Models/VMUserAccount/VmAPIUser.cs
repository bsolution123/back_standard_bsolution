﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DLL.Tables;

namespace API.Models.VMUserAccount
{
    public class Approved
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name Arabic Is Requried")]
        [StringLength(100)]
        public string FullNameAR { get; set; }
        [Required(ErrorMessage = "Name English Is Requried")]
        [StringLength(100)]
        public string FullNameEN { get; set; }
        [Required(ErrorMessage = "Mobile Is Requried")]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }
        [Required(ErrorMessage = "Email Is Requried")]
        [StringLength(100)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string Note { get; set; }
        public int? Role_Id { get; set; }
        public int? Department_Id { get; set; }
        public int? Position_Id { get; set; }

        public bool IsActive { get; set; }
        public int Status { get; set; }
        public string IPSignUp { get; set; }
    }
}