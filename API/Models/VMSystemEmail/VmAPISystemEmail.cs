﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models.VMSystemEmail
{
    public class VmAPISystemEmail
    {
        public int Id { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [MaxLength(400)]
        public string Email { get; set; }
        [Required]    
        [MaxLength(25)]
        public string Password { get; set; }
        [Required]
        [MaxLength(15)]
        public string SmtpPort { get; set; }
        [Required]
        [MaxLength(100)]
        public string SmtpServer { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}