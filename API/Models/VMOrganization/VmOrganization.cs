﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models.VMOrganization
{
    public class VmOrganization
    {
        [Key]
        public int Id { get; set; }
        [Required()]
        [MaxLength(100)]
        public string Org_Name { get; set; }
        [Required()]
        [MaxLength(100)]
        public string WorkedIn { get; set; }
        [Required()]
        [MaxLength(100)]
        public string Country { get; set; }
        [Required()]
        [MaxLength(100)]
        public string City { get; set; }
        [Required()]
        [MaxLength(500)]
        public string AddressInDetail { get; set; }
        [Required()]
        [MaxLength(100)]
        public string CommercialRegistrationNumber { get; set; }
        [Required()]
        [MaxLength(100)]
        public string TaxCard { get; set; }
        [Required()]
        [DataType(DataType.DateTime)]
        public DateTime EstablishmentDate { get; set; }
        public VmCompany Company { get; set; }
        public virtual ICollection<VmCompany> Companies { get; set; }
    }
}