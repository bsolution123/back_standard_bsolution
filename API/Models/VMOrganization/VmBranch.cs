﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models.VMOrganization
{
    public class VmBranch
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Branch Name Is Required")]
        [MaxLength(100)]
        [Display(Name = "Branch Name")]
        public string BranchName { get; set; }
        [Display(Name = "Main Branch")]
        public bool MainBranch { get; set; }
        [Required(ErrorMessage = "Address Is Required")]
        [MaxLength(100)]
        [Display(Name = "Address")]
        public string Address { get; set; }
    }
}