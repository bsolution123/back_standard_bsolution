﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models.VMOrganization
{
    public class VmCompany
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Company Name Is Required")]
        [MaxLength(100)]
        [Display(Name = "Company Name")]
        public string Comp_Name { get; set; }
        [Required(ErrorMessage = "Address Is Required")]
        [MaxLength(100)]
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "WorkedIn Is Required")]
        [MaxLength(100)]
        [Display(Name = "Worked In")]
        public string WorkedIn { get; set; }

        public virtual ICollection<VmBranch> Branchs { get; set; }
    }
}