﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models.VMOrganization
{
    public class VmBranch
    {
        [Key]
        public int Id { get; set; }
        [Required()]
        [MaxLength(100)]
        public string BranchName { get; set; }
        public bool? MainBranch { get; set; }
        [Required()]
        [MaxLength(100)]
        public string County { get; set; }
        [Required()]
        [MaxLength(100)]
        public string City { get; set; }
        [Required()]
        [MaxLength(100)]
        public string AddressInDetail { get; set; }
        public bool IsActive { get; set; }
        [Required()]
        [DataType(DataType.DateTime)]
        public DateTime EstablishmentDate { get; set; }
        public virtual VmCompany Company { get; set; }
        public int? MainBranch_Id { get; set; }
        public virtual VmBranch Branch { get; set; }
    }
}