﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models.VMOrganization
{
    public class VmCompany
    {
        [Key]
        public int Id { get; set; }
        [Required()]
        [MaxLength(100)]
        public string Comp_Name { get; set; }
        [Required()]
        [MaxLength(100)]
        public string Country { get; set; }
        [Required()]
        [MaxLength(100)]
        public string City { get; set; }
        [Required()]
        [MaxLength(100)]
        public string AddressInDetail { get; set; }
        [Required()]
        [MaxLength(100)]
        public string WorkedIn { get; set; }
        [Required()]
        [MaxLength(100)]
        public string CommercialRegistrationNumber { get; set; }
        [Required()]
        [MaxLength(100)]
        public string TaxCard { get; set; }
        [Required()]
        [DataType(DataType.DateTime)]
        public DateTime EstablishmentDate { get; set; }
        public int? Organization_Id { get; set; }
        public virtual ICollection<VmBranch> Branchs { get; set; }
        public virtual VmBranch Branch { get; set; }
        public virtual VmOrganization Organization { get; set; }
    }
}