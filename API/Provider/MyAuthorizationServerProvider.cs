﻿using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;
using DLL.Model_Context;
using DLL.Tables;
using System.Linq;
using DLL.Repository;
using System;

namespace API.Provider
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); // 
        }
       // private Repository<db_UserAccount> Rep = new Repository<db_UserAccount>();
        private Context db = new Context();
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            db_History_SignIn db_History = new db_History_SignIn();
    
            try
            {
                db_UserAccount User = db.users.Where(c => c.Password == context.Password && c.Email == context.UserName).FirstOrDefault();
               
                if (User !=null)
                {
                    var form = await context.Request.ReadFormAsync();

                    if (form != null)
                    {
                      db_History.IP = form["IPAddress"];
                        db_History.User_Id = User.Id;
                        db_History.Date = DateTime.Now;

                        db.History_SignIns.Add(db_History);

                        db.SaveChanges();


                        // mnay.IP_ID


                    }
                    //db.users.Add(getAuthUser)
                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.AddClaim(new Claim(ClaimTypes.Role, "admin"));
                    identity.AddClaim(new Claim("username", "admin"));
                    identity.AddClaim(new Claim(ClaimTypes.Name, "Sourav Mondal"));
                    context.Validated(identity);

                    return;
                }
                else
                {
                    context.SetError("invalid_grant", "Provided username and password is incorrect");
                    return;
                }
            }
            catch (Exception ex)
            {

                throw;
            }        
        }

    
    
    }
}